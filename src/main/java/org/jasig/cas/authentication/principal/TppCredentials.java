package org.jasig.cas.authentication.principal;

public class TppCredentials implements Credentials {
	private static final long serialVersionUID = 2053021031579470710L;  
	  
    private String idtype;  
  
    private String username;  
  
    private String password;

	public String getIdtype() {
		return idtype;
	}

	public void setIdtype(String idtype) {
		this.idtype = idtype;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}  

}
