package org.jasig.cas.authentication.principal;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class TppCredentialsToPrincipalResolver extends
AbstractPersonDirectoryCredentialsToPrincipalResolver {

	/** Logging instance. */
	private final Log log = LogFactory.getLog(getClass());


	public boolean supports(final Credentials credentials) {
		return credentials != null
				&& TppCredentials.class
						.isAssignableFrom(credentials.getClass());
	}

	@Override
	protected String extractPrincipalId(final Credentials credentials) {
		final TppCredentials tc = (TppCredentials) credentials;
        return tc.getUsername();
	}
}
